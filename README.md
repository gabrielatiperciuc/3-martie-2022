Pentru warm-up sa ne amintim de java vreau sa facem urmatorul exercitiu pana maine end of Day.

Console-Based app pentru filme.

Citesc de la tastatura o lista de Stringuri pana am empty string pe o linie –atunci ma opresc. 
Formatul –nume film an productie [newline]

------------
Ex:

    Lord of the rings 2001
    
	Star wars 1977
    
	Matrix 1999
    
	Blade Runner 2049 2017
    
----------

Dupa ce am introdus emty string pe o linie si am dat enter vreau sa se intample urmatoarele lucruri:

	1. Filmele sa fie salvate intr-o structura de date (set/lista/map -voi alegeti care se potriveste cel mai bine)
	2. Apoi vreau sa apara in consola un meniu cu 
		a.Cauta dupa an
		b.Cauta dupa cuvint cheie
		c.Afiseaza toate filmele in ordine cronologica
		d.Afiseaza toate filmele in ordine alfabetica
		
		
Daca aleg cauta dupa an (a) -atunci mi se cere sa introduc un an si mi se afiseaza filmele din anul respectiv, apoi cand apas enter vad din nou meniul 	

Daca aleg Cauta dupa civint cheie, mi se cere un cuvint cheie si mi se afiseaza toate filmelecare contin in titlul lor acel cuvint (case insensitive). Apoi cand apas enter vad din nou meniul

C si D sunt straightforward. La fel –la enter vad din nou meniul.
